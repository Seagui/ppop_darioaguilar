using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model
{

    /// <summary>
    /// Type of the Tile, Passable or not.
    /// </summary>
    public enum TileObjectType { Passable, Unpassable }

    /// <summary>
    /// Model of the Tiles.
    /// </summary>
    [CreateAssetMenu(fileName = "TileObject", menuName = "Objects/Tile")]
    public class TileObject : ScriptableObject
    {

        /// <summary>
        /// ID of the TileObject.
        /// </summary>
        public int ID;

        /// <summary>
        /// Cost of travel trought the Tile.
        /// </summary>
        public int travelCost;

        /// <summary>
        /// Type of the Tile, Passable or not.
        /// </summary>
        public TileObjectType type;

        /// <summary>
        /// Material of the Tile.
        /// </summary>
        public Material material;

    }

}
