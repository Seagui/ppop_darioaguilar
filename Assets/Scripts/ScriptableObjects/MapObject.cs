using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model
{

    /// <summary>
    /// Model of the Tiles.
    /// </summary>
    [CreateAssetMenu(fileName = "MapObject", menuName = "Objects/Map")]
    public class MapObject : ScriptableObject
    {

        /// <summary>
        /// Size in the X Coordinate.
        /// </summary>
        public int xSize;

        /// <summary>
        /// Size in the Y Coordinate.
        /// </summary>
        public int ySize;

        /// <summary>
        /// Map to generate in a text.
        /// </summary>
        public TextAsset textMap;

    }

}