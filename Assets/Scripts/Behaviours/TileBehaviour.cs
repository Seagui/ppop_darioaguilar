using Model;
using PathFinding;
using System.Collections.Generic;
using UnityEngine;
using View;

namespace Controller
{

    /// <summary>
    /// Behaviour of the tile
    /// </summary>
    public class TileBehaviour : MonoBehaviour, IAStarNode
    {

        public IEnumerable<IAStarNode> Neighbours { get { return neighbours; } }    // Neighbours Nodes

        [HideInInspector] public TileObject tile;   // Object of the tile

        [SerializeField] private string RisingUpTrigger;    //Trigger for RisingUp animation
        [SerializeField] private string DescendingTrigger;  //Triger for Descending animation

        private int x, z;                                                               // Coords of the tile
        private MapGenerator mapGenerator;                                              // Map Generator
        private readonly List<TileBehaviour> neighbours = new List<TileBehaviour>();    // List of the Neighbours Tiles

        private bool selected = false;  //Is the tile selected?

        void Awake() {
        
            // The Map Generator is found
            mapGenerator = GameObject.FindGameObjectWithTag("GameController").GetComponent<MapGenerator>();

            // A Listener to the Deselect Event is Setted
            mapGenerator.gameObject.GetComponent<PathsGenerator>().deselect.AddListener(SetDeselected);

        }

        public void SetTile(TileObject tile, int x, int z)
        {
            // The data of the tile is setted
            this.tile = tile;
            this.x = x;
            this.z = z;

            gameObject.name = "Tile " + z + ":" + x;

            gameObject.GetComponent<MeshRenderer>().material = tile.material;

            // The tile is activated
            gameObject.SetActive(true);

        }

        public float CostTo(IAStarNode neighbour)
        {
            var neigbourTile = neighbour as TileBehaviour;

            return neigbourTile.tile.travelCost;
        }

        public float EstimatedCostTo(IAStarNode target)
        {
            var targetTile = target as TileBehaviour;

            int currentX = x, currentZ = z;
            int targetX = targetTile.x, targetZ = targetTile.z;

            // The estimated cost is calculed, taking in acount the amount of tiles to travel and the average cost for tile
            int diferrence = Mathf.Abs((targetX - currentX)) + Mathf.Abs((targetZ - currentZ));
            float costTo = diferrence * mapGenerator.AverageTravelCosts();

            return costTo;
        }

        void OnTriggerEnter(Collider other)
        {

            // If the collider is from a tile, the tile is setted like a neighbour 
            if (other.CompareTag("Tile"))
                if(other.GetComponent<TileBehaviour>().tile.type == TileObjectType.Passable)
                    neighbours.Add(other.gameObject.GetComponent<TileBehaviour>());

        }

        public void SetSelected(bool principalNode)
        {

            if (!selected)
            {

                // The tile is elevated
                gameObject.GetComponent<Animator>().SetTrigger(RisingUpTrigger);

                // The highlighter material is created
                Material higlihterMatialCurrent = new Material(Shader.Find("Shader Graphs/HighlighterShaderGraph"));
                higlihterMatialCurrent.SetTexture("Texture2D_86637f0094aa4035905b4b786761c33c", tile.material.mainTexture);
                higlihterMatialCurrent.SetFloat("Vector1_6f4d3023c7f74c7ba715c7873b21f046", 0.2f);
                if (principalNode)
                    higlihterMatialCurrent.SetColor("Color_f78502755c024a8899adcfd90f82188d", mapGenerator.gameObject.GetComponent<PathsGenerator>().principalNodeColor);
                else
                    higlihterMatialCurrent.SetColor("Color_f78502755c024a8899adcfd90f82188d", mapGenerator.gameObject.GetComponent<PathsGenerator>().pathNodeColor);

                // The higlighter material is setted
                gameObject.GetComponent<MeshRenderer>().material = higlihterMatialCurrent;

                selected = true;
            }

        }

        private void SetDeselected()
        {

            if (selected)
            {
                // The tile is descended
                gameObject.GetComponent<Animator>().SetTrigger(DescendingTrigger);

                // The base material is setted
                gameObject.GetComponent<MeshRenderer>().material = tile.material;

                selected = false;
            }

        }

        // On a click, set the node like a principal node of a path
        private void OnMouseDown() => mapGenerator.gameObject.GetComponent<PathsGenerator>().SetNode(this);

    }

}