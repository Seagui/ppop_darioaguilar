using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controller
{

    /// <summary>
    /// Map generator class 
    /// </summary>
    public class MapGenerator : MonoBehaviour
    {

        [SerializeField] private int framesBetweenTiles = 0;    //Time in frames between the generation of two tiles
        [SerializeField] private float xSpaceBetweenTiles;      //Space in the X between the tiles
        [SerializeField] private float zSpaceBetweenTiles;      //Space in the Z between the tiles
        [SerializeField] private MapObject mapObject;           //Object that contains the map to generate
        [SerializeField] private List<TileObject> tileObjects;  //Objects that contains the types of tiles
        [SerializeField] private GameObject tilePrefab;         //Prefab of the tiles
        [SerializeField] private GameObject cam;                //Camera

        //The tiles are ordenated
        private void Awake() => OrdenateTiles();

        void Start() {

            //The map is generated
            StartCoroutine(GenerateMap());

            //The position for the cam is calculated
            CalculeCamPosition();

        }

        //The tiles are ordenated by their ID
        void OrdenateTiles() => tileObjects.Sort((a, b) => a.ID.CompareTo(b.ID));

        IEnumerator GenerateMap()
        {

            float zPosition = 0;
            int lineIndex = 0;
            //Foreach line in the textAsset
            foreach (string line in mapObject.textMap.text.Split('\n'))
            {

                float xPosition;
                //In case it is an odd row, the tiles are moved a little to the right
                if (lineIndex % 2 == 0)
                    xPosition = 0;
                else
                    xPosition = 0 + (xSpaceBetweenTiles / 2);

                int collumIndex = 0;
                //Foreach tile
                foreach (char tile in line.Trim().ToCharArray())
                {

                    //The tile is instantiated
                    GameObject tileObject = Instantiate(tilePrefab, transform);

                    //The position is given to the tile
                    tileObject.transform.position = new Vector3(xPosition, tileObject.transform.position.y, zPosition);

                    //The texture is setted to the tile
                    tileObject.GetComponent<TileBehaviour>().SetTile(tileObjects[(int)Char.GetNumericValue(tile)], collumIndex, lineIndex);

                    xPosition += xSpaceBetweenTiles;

                    collumIndex++;

                    for (int i=0; i<=framesBetweenTiles; i++)
                        yield return new WaitForEndOfFrame();
                }

                zPosition += zSpaceBetweenTiles;

                lineIndex++;
            }

        }

        void CalculeCamPosition()
        {

            //The position of the camera is calculated to be able to see the whole scene
            float x = ((xSpaceBetweenTiles * mapObject.xSize) - (xSpaceBetweenTiles / 2)) / 2;
            float z = zSpaceBetweenTiles * (mapObject.ySize - 1);
            float y = Mathf.Abs(Mathf.Sin(20) / (Mathf.Sin(70) / z));

            //The cam position is setted
            cam.transform.position = new Vector3(x, y, z);

        }

        /// <summary>
        /// Method that calculate the average travel cost of all tiles
        /// </summary>
        /// <returns>Average Travel Cost</returns>
        public int AverageTravelCosts()
        {
            int average = 0;

            foreach (TileObject tile in tileObjects)
                average += tile.travelCost;

            average /= tileObjects.Count;

            return average;
        }

    }

}