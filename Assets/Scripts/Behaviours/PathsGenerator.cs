using Controller;
using PathFinding;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace View
{

    /// <summary>
    /// Class to create paths
    /// </summary>
    public class PathsGenerator : MonoBehaviour
    {

        public Color principalNodeColor, pathNodeColor; // Color of the nodes

        [HideInInspector] public UnityEvent deselect;   // Event for deselect Nodes

        private TileBehaviour start, end;   // Tiles that mark the start and end of the path

        /// <summary>
        /// Method to set a Tile like Start or End Node
        /// </summary>
        /// <param name="tile">Tile to set like a Principal Node</param>
        public void SetNode(TileBehaviour tile)
        {

            // If the tile is passable
            if (tile.tile.type == Model.TileObjectType.Passable)
            {

                // If there isn't a Start tile
                if (!start)
                {
                    // Deselect all nodes
                    deselect.Invoke();

                    // Set the Start node like the tile
                    start = tile;
                    start.SetSelected(true);
                }
                else
                {

                    // If the tile is distint from the start node
                    if (tile != start)
                    {
                        // Set the end node like the tile
                        end = tile;

                        // Set the path
                        SetPath();
                    }

                }

            }
            else
                gameObject.GetComponent<ErrorText>().SetText("No puedes establecer como parte de tu viaje a una loseta de agua.");

        }

        void SetPath()
        {

            try
            {
                // Foreach node of the path
                IList<IAStarNode> list = AStar.GetPath(start, end);
                for (int i = 0; i < list.Count; i++)
                {
                    var targetTile = list[i] as TileBehaviour;

                    // If there isn't a principal node, set the node selected
                    if (targetTile != start && targetTile != end)
                        targetTile.SetSelected(false);

                }

                // Set the end node like selected
                end.SetSelected(true);

                // Forget the principal nodes
                start = null;
                end = null;
            }
            catch (NullReferenceException)
            {
                gameObject.GetComponent<ErrorText>().SetText("El destino seleccionado es inalcanzable.");
            }

        }

    }

}