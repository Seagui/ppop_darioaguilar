using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace View
{

    /// <summary>
    /// Behaviour of the error text
    /// </summary>
    public class ErrorText : MonoBehaviour
    {

        [SerializeField] private string animationTrigger;   //Trigger for the animation
        [SerializeField] private Text errorText;            //Error text
        [SerializeField] private Text backgroundText;       //Background text
        [SerializeField] private Animator animator;

        public void SetText(string text)
        {

            errorText.text = text;
            backgroundText.text = text;
            animator.SetTrigger(animationTrigger);

        }

    }

}